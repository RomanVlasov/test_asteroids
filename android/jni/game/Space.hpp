/*
 * Space.hpp
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef SPACE_HPP_
#define SPACE_HPP_

#include <vector>
#include <engine/render/Renderer_fwd.hpp>
#include "Space_fwd.hpp"

namespace game
{
	class Space
	{
	public:
		Space();

		void update(float timeSec);
		void render(engine::Renderer& renderer) const;

	private:
		using ObjectCollection = std::vector<SpaceObjectPtr>;
		ObjectCollection spaceObjects;
	};

} /* namespace game */
#endif /* SPACE_HPP_ */
