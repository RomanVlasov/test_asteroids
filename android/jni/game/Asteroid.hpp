/*
 * Asteroid.hpp
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef ASTEROID_HPP_
#define ASTEROID_HPP_

#include "SpaceObject.hpp"

namespace game {

class Asteroid: public game::SpaceObject
{
public:
	Asteroid();
};

} /* namespace game */
#endif /* ASTEROID_HPP_ */
