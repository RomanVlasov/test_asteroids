/*
 * Space.cpp
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#include "Space.hpp"
#include <engine/render/Renderer.hpp>

namespace game {

Space::Space()
{
	// TODO Auto-generated constructor stub

}

void Space::update(float timeSec)
{

}

void Space::render(engine::Renderer& renderer) const
{
	renderer.drawLine();
}

} /* namespace game */
