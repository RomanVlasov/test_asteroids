/*
 * SpaceObject.h
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef SPACEOBJECT_H_
#define SPACEOBJECT_H_

#include <vector>
#include <engine/Math_fwd.hpp>
#include <engine/render/Renderer_fwd.hpp>
#include "Space_fwd.hpp"


namespace game {

class SpaceObject
{
public:
	using Vector2d = engine::Vector2d;
	using DrawObjectCollection = std::vector<DrawObjectPtr>;

public:
	SpaceObject();
	virtual ~SpaceObject();

	void update(float timeSec);
	void render(DrawObjectCollection& drawObjects) const;
	void processCollision(SpaceObjectPtr object);

	void setVelocity(const Vector2d& velocity);
	void setPosition(const Vector2d& position);
	Vector2d getPosition() const;

protected:
	//Non-virtual interface idiom
	virtual void onUpdate(float timeSec) = 0;
	virtual void onCollide(SpaceObjectPtr otherObject) = 0;
	virtual void onRender(DrawObjectCollection& drawObjects) const = 0;

private:
	struct Implementation;
	using ImplType = std::unique_ptr<Implementation>;
	ImplType pImpl;

};

} /* namespace game */
#endif /* SPACEOBJECT_H_ */
