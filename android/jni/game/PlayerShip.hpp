/*
 * PlayerShip.hpp
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef PLAYERSHIP_HPP_
#define PLAYERSHIP_HPP_

#include "SpaceObject.hpp"

namespace game {

class PlayerShip: public game::SpaceObject {
public:
	PlayerShip();
};

} /* namespace game */
#endif /* PLAYERSHIP_HPP_ */
