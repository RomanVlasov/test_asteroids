/*
 * Space_fwd.hpp
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef SPACE_FWD_HPP_
#define SPACE_FWD_HPP_

#include <memory>

namespace game
{
	class Space;
	using SpacePtr = std::shared_ptr<Space>;
	using SpaceWeakPtr = std::weak_ptr<Space>;

	class SpaceObject;
	using SpaceObjectPtr = std::shared_ptr<Space>;
	using SpaceObjectWeakPtr = std::weak_ptr<Space>;

	class DrawObject;
	using DrawObjectPtr = std::shared_ptr<DrawObject>;
}


#endif /* SPACE_FWD_HPP_ */
