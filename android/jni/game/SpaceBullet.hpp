/*
 * SpaceBullet.h
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef SPACEBULLET_H_
#define SPACEBULLET_H_

#include "SpaceObject.hpp"

namespace game {

class SpaceBullet: public game::SpaceObject {
public:
	SpaceBullet();
};

} /* namespace game */
#endif /* SPACEBULLET_H_ */
