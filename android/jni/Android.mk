LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := game
LOCAL_CFLAGS    := -Wall -Wextra -DFREEGLUT_GLES
LOCAL_CPPFLAGS	:= -fexceptions
LOCAL_SRC_FILES :=  engine/Application.cpp \
					jni.cpp \
					engine/log/android/Log.cpp \
					engine/render/Renderer.cpp \
					engine/render/RendererOpenGL20.cpp \
					game/Asteroid.cpp \
					game/Space.cpp \
					game/SpaceObject.cpp
LOCAL_C_INCLUDES := engine/render/freeglut-gles/include
LOCAL_LDLIBS := -lGLESv2 -llog

include $(BUILD_SHARED_LIBRARY)