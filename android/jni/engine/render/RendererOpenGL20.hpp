/*
 * OpenGLES20Renderer.h
 *
 *  Created on: 14.11.2014
 *      Author: Roman
 */

#ifndef OPENGLES20RENDERER_H_
#define OPENGLES20RENDERER_H_

#include <memory>
#include "Renderer.hpp"

namespace engine
{
	class RendererOpenGL20 : public Renderer
	{
	public:
		RendererOpenGL20();
		virtual ~RendererOpenGL20() final;

		virtual void initialize() final;
		virtual void initializeFrameBuffers(int width, int height) final;
		virtual void drawLine() const;

	private:
		struct Implementation;
		using ImplType = std::unique_ptr<Implementation>;
		ImplType pImpl;
	};
}

#endif /* OPENGLES20RENDERER_H_ */
