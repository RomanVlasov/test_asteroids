/*
 * Renderer_fwd.hpp
 *
 *  Created on: 16.11.2014
 *      Author: Roman
 */

#ifndef RENDERER_FWD_HPP_
#define RENDERER_FWD_HPP_

#include <memory>

namespace engine
{
	class Renderer;
	using RendererPtr = std::unique_ptr<class Renderer>;
}


#endif /* RENDERER_FWD_HPP_ */
