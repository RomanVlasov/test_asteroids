/*
 * Renderer.h
 *
 *  Created on: 13.11.2014
 *      Author: Roman
 */

#ifndef RENDERER_H_
#define RENDERER_H_

#include <memory>

namespace engine
{
	struct Vector2d;

	class Renderer
	{
	public:
		using Ptr = std::unique_ptr<Renderer>;

	public:
		Renderer();
		virtual ~Renderer() = default;

		virtual void initialize() = 0;
		virtual void initializeFrameBuffers(int width, int height) = 0;
		virtual void drawLine() const = 0;
	};

}

#endif /* RENDERER_H_ */
