/*
 * OpenGLES20Renderer.cpp
 *
 *  Created on: 14.11.2014
 *      Author: Roman
 */

#include "RendererOpenGL20.hpp"
#include <engine/log/Log.hpp>
#include <glwrapper.h>


namespace engine
{
	GLint attribute_coord2d;
	GLuint program;

	struct RendererOpenGL20::Implementation
	{
		GLuint initVertexShader();
		GLuint initFragmentShader();
		GLuint createShaderProgram(GLuint vs, GLuint fs);
	};

	GLuint RendererOpenGL20::Implementation::initVertexShader()
	{
		GLint compile_ok = GL_FALSE;
		GLuint vs = glCreateShader(GL_VERTEX_SHADER);
		const char* glslSource =
				"#version 100\n"  // OpenGL ES 2.0
				"attribute vec2 coord2d;                  "
				"void main(void) {                        "
				"  gl_Position = vec4(coord2d, 0.0, 1.0); "
				"}";
		glShaderSource(vs, 1, &glslSource, NULL);
		glCompileShader(vs);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &compile_ok);
		if (!compile_ok)
		{
			throw std::runtime_error("Error in vertex shader\n");
		}
		return vs;
	}

	GLuint RendererOpenGL20::Implementation::initFragmentShader()
	{
		GLint compile_ok = GL_FALSE;
		GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
		const char *fs_source =
			"#version 100           \n"
			"void main(void) {        "
			"  gl_FragColor[0] = 0.0; "
			"  gl_FragColor[1] = 0.0; "
			"  gl_FragColor[2] = 1.0; "
			"}";
		glShaderSource(fs, 1, &fs_source, NULL);
		glCompileShader(fs);
		glGetShaderiv(fs, GL_COMPILE_STATUS, &compile_ok);
		if (!compile_ok)
		{
			throw std::runtime_error("Error in fragment shader\n");
		}
		return fs;
	}

	GLuint RendererOpenGL20::Implementation::createShaderProgram(GLuint vs, GLuint fs)
	{
		auto linkOk = GL_FALSE;
		auto program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);
		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &linkOk);
		if (!linkOk)
		{
			throw std::runtime_error("glLinkProgram error");
		}
		return program;
	}



	RendererOpenGL20::RendererOpenGL20()
	: pImpl( new Implementation() )
	{

	}

	RendererOpenGL20::~RendererOpenGL20()
	{
		//Empty for pImpl
	}

	void RendererOpenGL20::initialize()
	{
		glDisable(GL_SCISSOR_TEST);
		glDisable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
		glCullFace(GL_FRONT_AND_BACK);

		try
		{
			const auto vs = pImpl->initVertexShader();
			const auto fs = pImpl->initFragmentShader();
			program = pImpl->createShaderProgram(vs, fs);
			std::string attribute_name = "coord2d";
			attribute_coord2d = glGetAttribLocation(program, attribute_name.c_str());
			if (attribute_coord2d == -1)
			{
				throw std::runtime_error("Could not bind attribute " + attribute_name);
			}
		}
		catch(const std::runtime_error& error)
		{
			logError("[Error in OpenGL initialization] %s", error.what());
		}
	}

	GLuint viewRenderbuffer = 0;
	GLuint viewFramebuffer = 0;

	void RendererOpenGL20::initializeFrameBuffers(int width, int height)
	{
		glViewport(0, 0, width, height);

		glGenRenderbuffers(1, &viewRenderbuffer);

		glBindFramebuffer(GL_FRAMEBUFFER_OES, viewFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER_OES, viewRenderbuffer);

		//IOS Andalog
		//[pImpl->glContext renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(id<EAGLDrawable>)glView.layer];
		glRenderbufferStorage(GL_RENDERBUFFER_OES, GL_RGBA8_OES, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer);

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
		{
			//ENGINE_LOG_ERROR("Failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
			//return false;
		}

		//return true;
	}

	void RendererOpenGL20::drawLine() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER_OES, viewFramebuffer);

		/* Clear the background as white */
		  glClearColor(1.0, 0.0, 1.0, 1.0);
		  glClear(GL_COLOR_BUFFER_BIT);

		  glUseProgram(program);
		  glEnableVertexAttribArray(attribute_coord2d);
		  GLfloat triangle_vertices[] = {
		     0.0,  0.8,
		    -0.8, -0.8,
		     0.8, -0.8,
		  };
		  /* Describe our vertices array to OpenGL (it can't guess its format automatically) */
		  glVertexAttribPointer(
		    attribute_coord2d, // attribute
		    2,                 // number of elements per vertex, here (x,y)
		    GL_FLOAT,          // the type of each element
		    GL_FALSE,          // take our values as-is
		    0,                 // no extra data between each position
		    triangle_vertices  // pointer to the C array
		  );

		  /* Push each element in buffer_vertices to the vertex shader */
		  glDrawArrays(GL_TRIANGLES, 0, 3);
		  glDisableVertexAttribArray(attribute_coord2d);

		  /* Display the result */
		  //glutSwapBuffers();

		  glBindRenderbuffer(GL_RENDERBUFFER_OES, viewRenderbuffer);
	}

}

