#include <memory>

namespace engine
{
	class Application
	{
	public:
		static void onSurfaceCreated();
		static void onSurfaceChanged(int width, int height);
		static void onDrawFrame();

	private:
		struct Implementation;
		static Implementation& impl();

		using ImplType = std::unique_ptr<Implementation>;
		static ImplType pImpl;
	};
}
