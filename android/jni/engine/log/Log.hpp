//
//  CEngineLog.h
//  SkiResort
//
//  Created by Roman Vlasov on 6/5/12.
//  Copyright (c) 2012 Lypid. All rights reserved.
//

#ifndef _Core_CEngineLog_h_
#define _Core_CEngineLog_h_

#include <string>


namespace engine
{
	/*
	Format should be passed by value
	Because you cannot use references with va_star
	See: http://stackoverflow.com/questions/3184401/varargsva-list-va-start-doesnt-work-with-pass-by-reference-parameter
	*/
    void logInfo(std::string data, ...);
    void logError(std::string data, ...);
}


#endif
