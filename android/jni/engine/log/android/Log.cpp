/*
 * Log.cpp
 *
 *  Created on: 14.11.2014
 *      Author: Roman
 */

#include <engine/log/Log.hpp>
#include <string>
#include <android/log.h>


namespace engine
{
	//TODO: Think about buffer size
	static char buffer[0x8001];

    void logInfo(std::string data, ...)
	{
    	va_list args;
    	va_start(args, data);
    	vsprintf(buffer, data.c_str(), args);
    	va_end(args);

    	__android_log_print(ANDROID_LOG_INFO, "vlasov engine", "%s", buffer);
	}

    void logError(std::string data, ...)
    {
    	//TODO: Move to single function
    	va_list args;
		va_start(args, data);
		vsprintf(buffer, data.c_str(), args);
		va_end(args);

		__android_log_print(ANDROID_LOG_ERROR, "vlasov engine", "%s", buffer);
    }
}



