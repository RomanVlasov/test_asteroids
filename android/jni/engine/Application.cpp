#include "Application.hpp"
#include <engine/render/Renderer.hpp>
#include <engine/render/RendererOpenGL20.hpp>
#include "glwrapper.h"
#include <game/Space.hpp>


namespace engine
{
	Application::ImplType Application::pImpl;

	////////////////////////////////////////////
	// Implementation
	////////////////////////////////////////////
	struct Application::Implementation
	{
		Renderer::Ptr renderer;
		game::SpacePtr space;

		Implementation();
	};

	Application::Implementation::Implementation()
	: renderer(new RendererOpenGL20())
	, space(new game::Space())
	{
		//renderer->initialize();
	}

	////////////////////////////////////////////
	// Public functions
	////////////////////////////////////////////
	void Application::onSurfaceCreated()
	{
		impl().renderer->initialize();
	}

	void Application::onSurfaceChanged(int width, int height)
	{
		impl().renderer->initializeFrameBuffers(width, height);
		//impl().space->render(*impl().renderer);
	}

	void Application::onDrawFrame()
	{
		impl().space->render(*impl().renderer);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	Application::Implementation& Application::impl()
	{
		if (!pImpl)
		{
			pImpl.reset(new Implementation());
		}
		return *pImpl;
	}
}
